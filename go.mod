module github.com/jmichalak9/urlcollector

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.7.1
	github.com/stretchr/testify v1.7.0 // indirect
)

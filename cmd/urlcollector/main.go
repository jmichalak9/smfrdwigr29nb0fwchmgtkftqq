package main

import (
	"fmt"
	"log"

	"github.com/jmichalak9/urlcollector/pkg/providers"

	"github.com/gofiber/fiber/v2"

	"github.com/jmichalak9/urlcollector/pkg/urlcollector"
)

func main() {
	cfg, err := loadConfig()
	if err != nil {
		log.Fatalf("load config: %v", err)
	}

	collector := urlcollector.NewCollector(
		cfg.concurrentReq,
		providers.NewNASAProvider(cfg.apiKey),
	)

	app := fiber.New()

	app.Get("/pictures", collector.GetPicturesHandler)

	log.Fatal(app.Listen(fmt.Sprintf(":%d", cfg.port)))
}

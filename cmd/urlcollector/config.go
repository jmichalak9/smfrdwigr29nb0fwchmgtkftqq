package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type config struct {
	port          int
	apiKey        string
	concurrentReq int
}

func loadConfig() (*config, error) {
	portStr := strings.Trim(os.Getenv("PORT"), " ")
	fmt.Println(os.Getenv("PORT"))
	if portStr == "" {
		portStr = "8080"
	}
	port, err := strconv.Atoi(portStr)
	if err != nil {
		return nil, fmt.Errorf("cannot convert port env (%v) to int", portStr)
	}

	apiKey := strings.Trim(os.Getenv("API_KEY"), " ")
	if apiKey == "" {
		apiKey = "DEMO_KEY"
	}

	crStr := strings.Trim(os.Getenv("CONCURRENT_REQUESTS"), " ")
	if crStr == "" {
		crStr = "5"
	}
	cr, err := strconv.Atoi(crStr)
	if err != nil {
		return nil, fmt.Errorf("cannot convert concurent_request env (%v) to int", crStr)
	}

	return &config{
		port:          port,
		apiKey:        apiKey,
		concurrentReq: cr,
	}, nil
}

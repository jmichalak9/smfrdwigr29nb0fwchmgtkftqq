package providers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

type URLProvider interface {
	ProvideURL(ProviderQuery) (string, error)
}

type ProviderQuery struct {
	Date time.Time
}

type nasaProvider struct {
	apiKey  string
	baseURL string
}

func NewNASAProvider(apiKey string) *nasaProvider {
	return &nasaProvider{
		apiKey:  apiKey,
		baseURL: "https://api.nasa.gov/planetary/apod",
	}
}

type nasaResp struct {
	URL string `json:"url"`
}

func (n *nasaProvider) ProvideURL(q ProviderQuery) (string, error) {
	urlStr := fmt.Sprintf("%s?api_key=%s&date=%s",
		n.baseURL,
		url.QueryEscape(n.apiKey),
		url.QueryEscape(q.Date.Format("2006-01-02")),
	)
	resp, err := http.Get(urlStr)
	if err != nil {
		return "", fmt.Errorf("http get: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusTooManyRequests {
		return "", fmt.Errorf("too many requsts")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("read body: %v", err)
	}
	nasaResp := nasaResp{}
	if err := json.Unmarshal(body, &nasaResp); err != nil {
		return "", fmt.Errorf("unmarshal: %v", err)
	}
	if nasaResp.URL == "" {
		return "", fmt.Errorf("no url provided")
	}
	return nasaResp.URL, nil
}

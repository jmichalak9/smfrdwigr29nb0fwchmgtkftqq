package providers

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNasaProvider_ProvideURL(t *testing.T) {
	pastDate, _ := time.Parse("2006-01-02", "2021-03-04")

	tests := []struct {
		name    string
		handler http.Handler
		query   ProviderQuery
		expURL  string
		expErr  bool
	}{
		{
			name: "valid query",
			handler: http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
				rw.Write([]byte(`{"url": "example-url"}`))
			}),
			query: ProviderQuery{
				Date: pastDate,
			},
			expURL: "example-url",
		}, {
			name: "server returns internal error",
			handler: http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
				rw.WriteHeader(http.StatusInternalServerError)
			}),
			query: ProviderQuery{
				Date: pastDate,
			},
			expErr: true,
		}, {
			name: "server does not return url",
			handler: http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
				rw.Write([]byte(`{}`))
			}),
			query: ProviderQuery{
				Date: pastDate,
			},
			expErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.handler)
			defer server.Close()
			nasa := nasaProvider{
				baseURL: server.URL,
			}
			act, err := nasa.ProvideURL(tt.query)

			if tt.expErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			assert.Equal(t, tt.expURL, act)
		})
	}

}

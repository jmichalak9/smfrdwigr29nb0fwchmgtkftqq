package urlcollector

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/jmichalak9/urlcollector/pkg/providers"

	"github.com/gofiber/fiber/v2"
)

const dateFormat = "2006-01-02"

type Collector struct {
	jobChan chan job

	MaxConcurrentReq int
	Provider         providers.URLProvider
}

func NewCollector(maxConcurrentReq int, provider providers.URLProvider) *Collector {
	jobChan := make(chan job, 1000)

	for i := 0; i < maxConcurrentReq; i++ {
		go worker(provider, jobChan)
	}
	return &Collector{
		MaxConcurrentReq: maxConcurrentReq,
		Provider:         provider,
		jobChan:          jobChan,
	}
}

func (c *Collector) GetPicturesHandler(fc *fiber.Ctx) error {
	startDate := fc.Query("start_date")
	endDate := fc.Query("end_date")

	query, err := newCollectorQuery(startDate, endDate)
	if err != nil {
		return fc.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	urls, err := c.getPictures(*query)
	if err != nil {
		return fc.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
	return fc.JSON(fiber.Map{
		"urls": urls,
	})
}

func (c *Collector) getPictures(query collectorQuery) ([]string, error) {
	var urls []string
	var errors []string
	var wg sync.WaitGroup
	days := daysBetweenDates(query.startDate, query.endDate)
	resultChan := make(chan result, days)

	// add jobs
	for d := query.startDate; d.Before(query.endDate); d = d.AddDate(0, 0, 1) {
		c.jobChan <- job{
			query: providers.ProviderQuery{
				Date: d,
			},
			resultChan: resultChan,
			wg:         &wg,
		}
		wg.Add(1)
	}

	// process results
	wg.Wait()
	for i := 0; i < days; i++ {
		result := <-resultChan
		if result.err != nil {
			errors = append(errors, result.err.Error())
		} else {
			urls = append(urls, result.url)
		}
	}
	close(resultChan)
	if len(errors) > 0 {
		return nil, fmt.Errorf("%v", strings.Join(errors, ", "))
	}
	return urls, nil
}

func daysBetweenDates(a, b time.Time) int {
	if a.After(b) {
		a, b = b, a
	}
	return int(b.Sub(a).Hours() / 24)
}

package urlcollector

import (
	"fmt"
	"time"
)

type collectorQuery struct {
	startDate time.Time
	endDate   time.Time
}

func newCollectorQuery(start, end string) (*collectorQuery, error) {
	startDate, err := time.Parse(dateFormat, start)
	if err != nil {
		return nil, fmt.Errorf("parse start time: %+v", err)
	}

	endDate, err := time.Parse(dateFormat, end)
	if err != nil {
		return nil, fmt.Errorf("parse end time: %+v", err)
	}

	cq := &collectorQuery{
		startDate: startDate,
		endDate:   endDate,
	}
	if err := cq.validate(); err != nil {
		return nil, err
	}

	return cq, nil
}

func (q *collectorQuery) validate() error {
	if q.startDate.After(q.endDate) {
		return fmt.Errorf("start date must be before end date")
	}
	if q.startDate.Truncate(24 * time.Hour).After(time.Now().Truncate(24 * time.Hour)) {
		return fmt.Errorf("start date is after today")
	}
	return nil
}

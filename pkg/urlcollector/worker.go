package urlcollector

import (
	"fmt"
	"sync"

	"github.com/jmichalak9/urlcollector/pkg/providers"
)

type job struct {
	query      providers.ProviderQuery
	wg         *sync.WaitGroup
	resultChan chan result
}

type result struct {
	url string
	err error
}

func worker(provider providers.URLProvider, jobs <-chan job) {
	for job := range jobs {
		url, err := provider.ProvideURL(job.query)
		if err != nil {
			job.resultChan <- result{
				err: fmt.Errorf("provide url: %v", err),
			}
		} else {
			job.resultChan <- result{
				url: url,
			}
		}
		job.wg.Done()
	}
}

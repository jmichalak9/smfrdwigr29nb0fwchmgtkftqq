FROM golang:1.16-alpine as build

RUN apk update && apk add --no-cache gcc musl-dev
WORKDIR /app
COPY ./ .
RUN go build \
	-a -o dist/urlcollector \
	./cmd/urlcollector


FROM alpine:3.13
ENV API_KEY="DEMO_KEY"
ENV CONCURRENT_REQUESTS=5
ENV PORT=8080
COPY --from=build /app/dist/urlcollector /app/urlcollector
WORKDIR /app
EXPOSE ${PORT}
CMD ["sh", "-c", "API_KEY=${API_KEY} CONCURRENT_REQUESTS=${CONCURRENT_REQUESTS} PORT=${PORT} /app/urlcollector"]